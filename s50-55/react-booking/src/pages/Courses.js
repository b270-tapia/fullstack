import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'

export default function Courses() {
	// console.log(coursesData);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([])
	// "map" method loops through the individual courrse in our mock database and returns a CourseCard components for each course
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 	)
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return(
					<CourseCard key={course.id} courseProp={course}/>
				)
			}))
		})

	}, [])
	
	return (

		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	)
}